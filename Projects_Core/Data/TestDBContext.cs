using Microsoft.EntityFrameworkCore;
using System;
using Projects_Core.Data.Models;
namespace Projects_Core.Data
{
    public class TestDBContext : DbContext
    {
        public TestDBContext(DbContextOptions options) : base(options)
        {
        }

        public virtual DbSet<User> User{get;set;}
        protected override void OnModelCreating(ModelBuilder modelB)
        {
            new User.Map(modelB.Entity<User>().ToTable("user"));

            base.OnModelCreating(modelB);
        } 
    }
}