using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Linq;

namespace Projects_Core.Data.Models
{
    public class User
    {
        public int Id {get; set;}
        public string Name {get; set;}
        public string Password {get;set;}
        public Boolean Estatus {get;set;}

        public class Map{
            public Map(EntityTypeBuilder<User> eb)
            {
                eb.HasKey(x => x.Id);
                eb.Property(x => x.Name).HasColumnType("varchar").HasMaxLength(100);
                eb.Property(x => x.Password).HasColumnType("varchar").HasMaxLength(200);
                eb.Property(x => x.Estatus).HasColumnType("bit");
            }
        }
    }
}