using System;
using Projects_Core.Data.Models;
using Projects_Core.Data;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Projects_Core.Services
{
    public class UserServices
    {
        private readonly TestDBContext _testDBContext;
        public UserServices(TestDBContext testDBContext)
        {
            _testDBContext = testDBContext;
        }

        public List<User> getUsers()
        {
            try{
                var result = _testDBContext.User.ToList();
                return result;
            }
            catch{
                return new List<User>();
            }
        }
    }
}