using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using Projects_Core.Data.Models;
using Projects_Core.Data;
using Projects_Core.Services;
using System.Collections.Generic;

namespace Projects_Core.Controllers
{
    public class UserController : Controller
    {
        private readonly UserServices _userService;
        public UserController(UserServices userServices)
        {
            _userService = userServices;
        }

        public IActionResult Index ()
        {
            var result = _userService.getUsers();
            return View(result);
        }
        
    }
}